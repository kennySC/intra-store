(self["webpackChunkintra_store"] = self["webpackChunkintra_store"] || []).push([["src_app_init-options_init-options_module_ts"],{

/***/ 8682:
/*!*************************************************************!*\
  !*** ./src/app/init-options/init-options-routing.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "InitOptionsPageRoutingModule": () => (/* binding */ InitOptionsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _init_options_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./init-options.page */ 5666);




const routes = [
    {
        path: '',
        component: _init_options_page__WEBPACK_IMPORTED_MODULE_0__.InitOptionsPage
    }
];
let InitOptionsPageRoutingModule = class InitOptionsPageRoutingModule {
};
InitOptionsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], InitOptionsPageRoutingModule);



/***/ }),

/***/ 126:
/*!*****************************************************!*\
  !*** ./src/app/init-options/init-options.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "InitOptionsPageModule": () => (/* binding */ InitOptionsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _init_options_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./init-options-routing.module */ 8682);
/* harmony import */ var _init_options_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./init-options.page */ 5666);







let InitOptionsPageModule = class InitOptionsPageModule {
};
InitOptionsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _init_options_routing_module__WEBPACK_IMPORTED_MODULE_0__.InitOptionsPageRoutingModule
        ],
        declarations: [_init_options_page__WEBPACK_IMPORTED_MODULE_1__.InitOptionsPage]
    })
], InitOptionsPageModule);



/***/ }),

/***/ 5666:
/*!***************************************************!*\
  !*** ./src/app/init-options/init-options.page.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "InitOptionsPage": () => (/* binding */ InitOptionsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_init_options_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./init-options.page.html */ 6341);
/* harmony import */ var _init_options_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./init-options.page.scss */ 3393);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);




let InitOptionsPage = class InitOptionsPage {
    constructor() {
        this.logoSrc = "";
    }
    ngOnInit() {
        this.logoSrc = "../../assets/logos/logo.png";
    }
};
InitOptionsPage.ctorParameters = () => [];
InitOptionsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-init-options',
        template: _raw_loader_init_options_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_init_options_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], InitOptionsPage);



/***/ }),

/***/ 3393:
/*!*****************************************************!*\
  !*** ./src/app/init-options/init-options.page.scss ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("#container {\n  background: #211b8b !important;\n  background: linear-gradient(180deg, #211b8b 0%, #4343b0 35%, #0a87a1 100%) !important;\n  height: 100vh !important;\n  display: flex;\n  flex-direction: column;\n  flex-wrap: nowrap;\n  justify-content: center;\n}\n\n#logoImg {\n  background-color: transparent !important;\n}\n\n.btn_init {\n  width: 75% !important;\n  align-self: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluaXQtb3B0aW9ucy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDSSw4QkFBQTtFQUNBLHFGQUFBO0VBQ0Esd0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxpQkFBQTtFQUNBLHVCQUFBO0FBREo7O0FBSUE7RUFDSSx3Q0FBQTtBQURKOztBQUlBO0VBQ0kscUJBQUE7RUFDQSxrQkFBQTtBQURKIiwiZmlsZSI6ImluaXQtb3B0aW9ucy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuXHJcbiNjb250YWluZXIge1xyXG4gICAgYmFja2dyb3VuZDogcmdiKDMzLDI3LDEzOSkgIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgxODBkZWcsIHJnYmEoMzMsMjcsMTM5LDEpIDAlLCByZ2JhKDY3LDY3LDE3NiwxKSAzNSUsIHJnYmEoMTAsMTM1LDE2MSwxKSAxMDAlKSAhaW1wb3J0YW50O1xyXG4gICAgaGVpZ2h0OiAxMDB2aCAhaW1wb3J0YW50O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBmbGV4LXdyYXA6IG5vd3JhcDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcblxyXG4jbG9nb0ltZyB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uYnRuX2luaXQge1xyXG4gICAgd2lkdGg6IDc1JSAhaW1wb3J0YW50O1xyXG4gICAgYWxpZ24tc2VsZjogY2VudGVyO1xyXG59Il19 */");

/***/ }),

/***/ 6341:
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/init-options/init-options.page.html ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content>\n\n  <div id=\"container\" class=\"ion-text-center\">\n\n    <!-- <ion-item> -->\n      <ion-img id=\"logoImg\" [src]=\"logoSrc\"></ion-img>      \n    <!-- </ion-item> -->\n    \n    <!-- <h4>Bienvenido</h4>     -->\n    <h4> Ya eres usuario?</h4>\n    <ion-button class=\"btn_init\" color=\"success\" fill=\"solid\" shape=\"round\" [routerLink]=\"['/login']\">\n      Ingresar\n    </ion-button>\n    <h4> O </h4>\n    <ion-button class=\"btn_init\" color=\"secondary\" fill=\"solid\" shape=\"round\" [routerLink]=\"['/register']\">\n      Registrarse\n    </ion-button>    \n    \n  </div>\n\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=src_app_init-options_init-options_module_ts.js.map