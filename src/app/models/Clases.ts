export class ConfigSite {
    webApiUrl: string;
}

export class Usuario {
    public id: number ;
    public email: string ;
    public nombre: string ;
    public password: string ;
    public telefono: string ;
    public isActivo: boolean ;
}

export class Tienda {
    public id : number;
    public idAministrador : number;
    public nombre : string;
    public telefono : string;
    public email : string;
    public ubicacion : string;
    public isActivo : boolean;
}