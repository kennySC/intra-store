import { Tienda, Usuario } from "../models/Clases";

export class Status {
    public code: number;
    public message: string;
}

export class UsuarioLoginResponse {
    public status: Status;
    public Data: Usuario;
}

export class TiendasGetResponse {
    public status: Status;
    public Data: Tienda[];
}