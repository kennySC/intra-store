import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { ConfigSite, Usuario } from 'src/app/models/Clases';
import { ConfigService } from 'src/app/services/config.service';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {


  logoSrc: string = "";
  sending: boolean = false;
  usuario: Usuario;
  configData: ConfigSite;
  email: string = "";
  password: string = "";
  logedUser: Usuario;

  constructor(private router: Router, 
              private usuariosServ: UsuariosService, 
              private configServ: ConfigService,
              private alertController: AlertController,
              private loadingController: LoadingController) { }

  ngOnInit() {            

    this.logoSrc = "../../assets/logos/logov4.png";

    this.logedUser = JSON.parse(localStorage.getItem("usuario"));
    
    if (this.logedUser != null && this.logedUser.id > 0) {
      
      this.router.navigate(['/home']);      

    } else {

      this.configServ.getConfig().subscribe(
        resp => {
          this.configData = resp;
        },
        error => {
          console.log('Error get config');
        },
        () => {
          localStorage.clear();
        }      
      );
      
    } 

  }

  onSubmit() {    
    this.sending = true;
    this.usuariosServ.usuarioLogin(this.configData.webApiUrl, this.email, this.password).subscribe(
      resp => {        
        console.log(resp);
        this.sending = false;
        if (resp.status.code == 0) {                                       
          localStorage.setItem("usuario", JSON.stringify(resp.Data));
          this.router.navigate(['/home']);
        } 
        else if (resp.status.code == -1) {
          this.showAlertOk('Error', 'usuario no encontrado', '¡por favor, revisa los datos de inicio!');
        } else {
          this.showAlertOk('Error', '', 'Lo sentimos, algo salío mal');
        }
      },
      error => {        
        this.sending = false;
        this.showAlertOk('Error', '', 'Lo sentimos, algo salío mal');
      },
      () => {        
        this.sending = false;
      }   
    );

  }

  goBack() {
    this.router.navigate(['/']);
  }

  goRegister() {
    this.router.navigate(['/register']);
  }

  async showAlertOk(title: string, subtitle: string, message: string) {
    const alert = await this.alertController.create({
      cssClass: '',
      header: title,
      subHeader: subtitle,
      message: message,
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async showLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Espera por favor...',      
    });
    await loading.present();
        
  }

}
