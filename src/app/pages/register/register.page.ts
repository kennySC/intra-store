import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { ConfigSite, Usuario } from 'src/app/models/Clases';
import { ConfigService } from 'src/app/services/config.service';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  configData: ConfigSite;
  usuario: Usuario;
  name: string = "";
  email: string = "";
  password: string = "";
  confirm_password: string = "";
  phoneNumber: string = "";

  constructor(private router: Router,
              private configServ: ConfigService,
              private usuarioServ: UsuariosService,
              private alertController: AlertController,
              private loadingController: LoadingController) { }

  ngOnInit() {
    
    this.usuario = new Usuario();
    
    this.configServ.getConfig().subscribe(
      resp => {
        this.configData = resp;
      },
      error => {
        console.log('Error get config');
      },
      () => {
        localStorage.clear();
      }      
    );

  }

  onSubmit() {
    
    if (this.usuario && this.usuario != null) {
      
      if ((this.usuario.nombre != null && this.usuario.nombre != '')
            && (this.usuario.email != null && this.usuario.email != '')
            && (this.usuario.telefono != null && this.usuario.telefono != '')
            && (this.usuario.password != null && this.usuario.password != '') 
            && (this.confirm_password != '' && this.usuario.password == this.confirm_password) ) {
        
            this.usuario.isActivo = true;
            this.usuarioServ.registerUsuario(this.configData.webApiUrl, this.usuario).subscribe(
              resp => {
                if (resp.code == 0) {
                  this.showAlertOk('Exito!', '', '¡Ya puedes ingresar a Intra Store!');
                  this.router.navigate(['/']);
                } else {
                  this.showAlertOk('Exito!', '', 'Lo sentimos, algo salió mal');
                }
              },
              error => {
                this.showAlertOk('Exito!', '', 'Lo sentimos, algo salió mal');
              }

            );

      } else {

        this.showAlertOk('Error', '', '¡Por favor, complete los datos!');

      }

    }

  }

  goLogin() {
    this.router.navigate(['/']);
  }


  async showAlertOk(title: string, subtitle: string, message: string) {
    const alert = await this.alertController.create({
      cssClass: '',
      header: title,
      subHeader: subtitle,
      message: message,
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async showLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Espera por favor...',      
    });
    await loading.present();
        
  }

}
