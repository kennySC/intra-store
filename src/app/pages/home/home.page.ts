import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { ConfigSite, Tienda, Usuario } from 'src/app/models/Clases';
import { ConfigService } from 'src/app/services/config.service';
import { TiendasService } from 'src/app/services/tiendas.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  configData: ConfigSite;

  usuario: Usuario;
  tiendas: Tienda[];

  constructor(private router: Router,
              private configServ: ConfigService,
              private tiendaServ: TiendasService,
              private loadingController: LoadingController,
              private alertController: AlertController) { }

  ngOnInit() {   
    
    this.usuario = JSON.parse(localStorage.getItem("usuario"));    
    
    if (this.usuario == null || this.usuario.id <= 0) {      
      localStorage.clear();
      this.router.navigate(['/']);
    } else {      
      this.configServ.getConfig().subscribe(
        resp => {
          // this.loadingController.dismiss();
          this.configData = resp;       
          this.getActiveStores();   
        },
        error => {          
          console.log('Error get config');
        },
        () => {        
          localStorage.clear();
        }      
      );

    }

  }

  // Metodo para traer las tiendas 
  getActiveStores() {  
    this.tiendaServ.getActiveTiendas(this.configData.webApiUrl).subscribe(
      resp => {        
        if (resp.status.code == 0) {
          this.tiendas = resp.Data;          
        }
        else if (resp.status.code == -1) {
          this.showAlertOk('Error', '', 'sin tiendas que cargar');          
        }
        else {
          this.showAlertOk('Error', '', 'lo sentimos, algo salío mal_1');
        }
      },
      error => {
        this.showAlertOk('Error', '', 'lo sentimos, algo salío mal_2');        
      }

    );
  }


  async showAlertOk(title: string, subtitle: string, message: string) {
    const alert = await this.alertController.create({
      cssClass: '',
      header: title,
      subHeader: subtitle,
      message: message,
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async showLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Espera por favor...',      
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

}
