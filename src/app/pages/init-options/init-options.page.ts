import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/models/Clases';

@Component({
  selector: 'app-init-options',
  templateUrl: './init-options.page.html',
  styleUrls: ['./init-options.page.scss'],
})
export class InitOptionsPage implements OnInit {

  logoSrc: string = "";
  logedUser: Usuario;

  constructor(private router: Router) { }

  ngOnInit() {    
    // this.logoSrc = "../../assets/logos/logo.png";
    this.logoSrc = "../../assets/logos/logov4.png";

    this.logedUser = JSON.parse(localStorage.getItem("usuario"));

    if (this.logedUser != null && this.logedUser.id > 0) {
      this.router.navigate(['/home']);
    }

  }

}
