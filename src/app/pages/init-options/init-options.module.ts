import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InitOptionsPageRoutingModule } from './init-options-routing.module';

import { InitOptionsPage } from './init-options.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InitOptionsPageRoutingModule
  ],
  declarations: [InitOptionsPage]
})
export class InitOptionsPageModule {}
