import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import 'rxjs/Rx';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { TiendasGetResponse } from "../DTOS/DTO";

@Injectable({
  providedIn: 'root'
})
export class TiendasService {

  constructor(private _http: HttpClient) { }

  getActiveTiendas(apiUrl: string): Observable<TiendasGetResponse> {
    return this._http.get(apiUrl + `Tiendas/getActiveTiendas`)
      .map((resp: any) => <TiendasGetResponse>resp)
      .catch((error: any) => Observable.throwError(error || 'Server Error'));
  };  

}
