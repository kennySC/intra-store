import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import 'rxjs/Rx';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { Status, UsuarioLoginResponse } from "../DTOS/DTO";
import { Usuario } from '../models/Clases';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(private _http: HttpClient) { }

  usuarioLogin(apiUrl: string, email: string, password: string): Observable<UsuarioLoginResponse> {
    return this._http.get(apiUrl + `Usuarios/login/${email}/${password}`)
      .map((resp: any) => <UsuarioLoginResponse>resp)
      .catch((error: any) => Observable.throwError(error || 'Server Error'));
  };

  registerUsuario(apiUrl: string, usuario: Usuario): Observable<Status> {
    return this._http.post(apiUrl + `Usuarios/register`, usuario)
      .map((resp: any) => <Status>resp)
      .catch((error: any) => Observable.throwError(error || 'Server Error'));
  }
  
}